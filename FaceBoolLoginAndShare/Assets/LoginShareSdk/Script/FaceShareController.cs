using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceShareController
{
    private Action<bool, string> _ShareCallback = null;
    private Action<bool, string> _ShareImageCallback = null;
    public void ShareLink(string LinkUrl, Action<bool, string> HandleCallBack)
    {
        _ShareCallback = HandleCallBack;
        //Uri("https://developers.facebook.com/")
        FB.ShareLink(new Uri(LinkUrl), "", "", new Uri(LinkUrl), callback: HandleResult);
    }

    public void ShareLink(string LinkUrl, string Title, string Description, string ImageUrl, Action<bool, string> HandleCallBack)
    {
        _ShareCallback = HandleCallBack;
        FB.ShareLink(new Uri(LinkUrl), Title, Description, new Uri(ImageUrl), callback: HandleResult);
    }
    public void FeedShare(string toId = "", Uri link = null, Uri picture = null, string mediaSource = "", Action<bool, string> HandleCallBack = null)
    {
        _ShareCallback = HandleCallBack;
        FeedShare(toId, link, "", "", "", picture, mediaSource, HandleResult);

    }

    public void ShareImageToFaceBook(Texture2D tex, Action<bool, string> ShareImageCallback)
    {
        _ShareImageCallback = ShareImageCallback;
        byte[] screenshot = tex.EncodeToPNG();

        var wwwForm = new WWWForm();
        wwwForm.AddBinaryData("image", screenshot, "InteractiveConsole.png");
        wwwForm.AddField("message", "herp derp.  I did a thing!  Did I do this right?");
        FB.API("me/photos", HttpMethod.POST, this.HandleImageResult, wwwForm);
    }
    protected void HandleImageResult(IResult result)
    {
        if (result == null)
        {
            if (_ShareImageCallback != null)
            {
                _ShareImageCallback(false, "Null Response");
            }
            return;
        }

        string statueInfo = "";
        // Some platforms return the empty string instead of null.
        if (!string.IsNullOrEmpty(result.Error))
        {
            statueInfo = "Error Response:" + result.Error;
        }
        else if (result.Cancelled)
        {
            statueInfo = "Cancelled Response:" + result.RawResult;
        }
        else if (!string.IsNullOrEmpty(result.RawResult))
        {
            statueInfo = "Success Response:" + result.RawResult;
            if (_ShareImageCallback != null)
                _ShareImageCallback(true, statueInfo);
        }
        else
        {
            statueInfo = "";
        }
        if (_ShareImageCallback != null)
            _ShareImageCallback(false, statueInfo);
    }
    private void FeedShare(string toId = "", Uri link = null, string linkName = "", string linkCaption = "", string linkDescription = "", Uri picture = null, string mediaSource = "", FacebookDelegate<IShareResult> callback = null)
    {
        FB.FeedShare(
            toId,
            link,
            linkName,
            linkCaption,
            linkDescription,
            picture,
            mediaSource,
            callback);
    }

    protected void HandleResult(IResult result)
    {
        if (result == null)
        {
            if (_ShareCallback != null)
            {
                _ShareCallback(false, "Null Response");
            }
            return;
        }

        string statueInfo = "";
        // Some platforms return the empty string instead of null.
        if (!string.IsNullOrEmpty(result.Error))
        {
            statueInfo = "Error Response:" + result.Error;
        }
        else if (result.Cancelled)
        {
            statueInfo = "Cancelled Response:" + result.RawResult;
        }
        else if (!string.IsNullOrEmpty(result.RawResult))
        {
            statueInfo = "Success Response:" + result.RawResult;
            if (_ShareCallback != null)
                _ShareCallback(true, statueInfo);
        }
        else
        {
            statueInfo = "";
        }
        if (_ShareCallback != null)
            _ShareCallback(false, statueInfo);
    }
}
