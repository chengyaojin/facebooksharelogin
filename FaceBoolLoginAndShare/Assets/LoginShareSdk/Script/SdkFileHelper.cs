﻿using System;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Text;

public static class SdkFileHelper
{

    public const string fileName = "localRoomData";
    public static string PathToURI(string path)
    {
        return "file://" + path;
    }

    public static void SaveTexture(Texture2D texture, string fileName)
    {
        byte[] png = texture.EncodeToPNG();
        //System.IO.File.WriteAllBytes(Application.persistentDataPath + "/" +fileName, png);
        SdkFileHelper.SaveBytesDataToLocal(Application.persistentDataPath + "/" + fileName, png);
        //DebugTools.Log(Application.persistentDataPath);
    }
    public static string persistentDataPath = "";
    public static string getPersistentDataPath()
    {
        if (persistentDataPath == "")
            persistentDataPath = Application.persistentDataPath;
        return persistentDataPath;
    }
    public static Texture2D ReadTexture(string fileName)
    {
        string path = Application.persistentDataPath + "/" + fileName + ".png";
        if(!File.Exists(path))
            return null;
        byte[] bts = File.ReadAllBytes(path);
        Texture2D tex = new Texture2D(0, 0, TextureFormat.ARGB32, false);
        tex.LoadImage(bts);
        return tex;
    }

    public static string ShareTexturePath(string texName)
    {
        return Application.persistentDataPath + "/share/" + texName + ".jpg";
    }

    public static string ShareDirectory()
    {
        return Application.persistentDataPath + "/share/";
    }



    public static string SaveToShareTexture(Texture2D texture)
    {
        byte[] jpgByte = texture.EncodeToJPG();
        string path = ShareTexturePath(texture.name);
        //System.IO.File.WriteAllBytes(path, jpgByte);
        SaveBytesDataToLocal(path, jpgByte);
        //DebugTools.Log(path);
        return path;
    }

    public static void DeleteSharedTexture(string path)
    {
        if(File.Exists(path))
        {
            File.Delete(path);
            //DebugTools.Log("delete file :" + path);
        }
    }

    public static void ShowAllTextureInPersistencePath()
    {
        System.IO.DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        int count = 0;
        foreach(var v in di.GetFiles())
        {
            if(v != null && v.FullName.EndsWith(".png"))
            {
                //DebugTools.Log("png file:" + v.Name);
                count++;
            }
        }
        //DebugTools.Log("png count: " + count);
    }

    /// <summary>
    /// 将byte数据写入本地
    /// </summary>
    /// <param name="path"></param>
    /// <param name="bytes"></param>
    public static void SaveBytesDataToLocal(string path, byte[] bytes)
    {
#if UNITY_EDITOR
        File.WriteAllBytes(path, bytes);
#else
        try
        {
            File.WriteAllBytes(path, bytes);
        }
        catch (Exception ex)
        {
            throw new Exception(string.Format("空间不足： {0}", ex.ToString()));
        }
#endif
        //DebugTools.Log("write file to path:" + path);
    }


    public static void SaveTextDataToLocal(string path, string jsonData)
    {
#if UNITY_EDITOR
        File.WriteAllText(path, jsonData);
#else
        try
        {
            File.WriteAllText(path, jsonData);
        }
        catch (Exception ex)
        {
            throw new Exception(string.Format("空间不足： {0}", ex.ToString()));
            //UIPage.ShowPage<UINoneSpaceTip>(132);
        }
#endif
    }
    public static string LoadTextDataFromLocal(string path)
    {
        string jsonData = "";
        if (File.Exists(path))
        {
            jsonData = System.IO.File.ReadAllText(path);
        }
        return jsonData;
    }
    public static string LevelDataPath(string roomName)
    {
        return Application.persistentDataPath + "/" + roomName + ".dat";
    }

    public static string LoadLevelDataString(string roomName)
    {
        string filePath = LevelDataPath(roomName);
        if(File.Exists(filePath))
        {
            string opJson = System.IO.File.ReadAllText(filePath);
            return opJson;
        }
        return null;
    }

    public static void SaveLevelDataString(string roomName, string dataStr)
    {
        string filePath = LevelDataPath(roomName);
        SaveTextDataToLocal(filePath, dataStr);
        //DebugTools.Log("Save Level : " + roomName + ", Path: " + filePath);
    }


    public static void DeleteLocalData()
    {
        string filePath = LevelDataPath(fileName);
        if(File.Exists(filePath))
        {
            File.Delete(filePath);
        }
    }

    ///存储本地数据
    public static void SaveDataToJson<T>(T t,string fileName)
    {
        string path = LevelDataPath(fileName);
        string json = JsonUtility.ToJson(t);
        File.WriteAllText(path, json);
    }

    ///读取本地存储数据
    public static T LoadDataFromJson<T>( string fileName)
    {
        string filePath = LevelDataPath(fileName);
        if (!File.Exists(filePath)) return default(T);
        string json = File.ReadAllText(filePath);
        return JsonUtility.FromJson<T>(json);
    }



    //facebook 相关
    public static string FaceBookPlayerInfoPath()
    {
        return getPersistentDataPath() + "/";
    }
    public static string GetFaceBookImagePath(string id)
    {
        string path = FaceBookPlayerInfoPath() + id + ".png";
        return path;
    }
    public static Texture2D ReadFaceBookTexture(string fileName)
    {
        string path = GetFaceBookImagePath(fileName);
        if (!File.Exists(path))
            return null;
        byte[] bts = File.ReadAllBytes(path);
        Texture2D tex = new Texture2D(0, 0, TextureFormat.ARGB32, false);
        tex.LoadImage(bts);
        return tex;
    }
    public static void SaveFaceBookTexture(Texture2D texture, string fileName)
    {
        byte[] png = texture.EncodeToPNG();
        SdkFileHelper.SaveBytesDataToLocal(GetFaceBookImagePath(fileName), png);
    }
    public static void SaveFaceBookDataToJson<T>(T t, string fileName)
    {
        string path = FaceBookPlayerInfoPath() + fileName;
        string json = JsonUtility.ToJson(t);
        File.WriteAllText(path, json);
    }

    public static T LoadFaceBookDataFromJson<T>(string fileName)
    {
        string filePath = FaceBookPlayerInfoPath() + fileName;
        if (!File.Exists(filePath)) return default(T);
        string json = File.ReadAllText(filePath);
        return JsonUtility.FromJson<T>(json);
    }

}
