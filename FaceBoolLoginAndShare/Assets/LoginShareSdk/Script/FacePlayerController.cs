using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//处理缓存信息，玩家头像下载等功能都在这里面
public class FacePlayerController 
{
    readonly string FaceBookLocalFileName = "FaceBookInfo.dat";


    private Action<bool, string> _GetMeInfoCallback     = null;
    private ResponseInfo _MySelfInfos                   = null;
    private List<FaceBookPlayerInfo> faceBookFriendList = new List<FaceBookPlayerInfo>();
    private FaceBookPlayerInfo selfInfo                 = new FaceBookPlayerInfo();

    //初始化完成后，可以先把好友列表及个人信息初始化
    public void InitFriendList()
    {
        if (_MySelfInfos == null)
        {
            _MySelfInfos = SdkFileHelper.LoadFaceBookDataFromJson<ResponseInfo>(FaceBookLocalFileName);

            if (_MySelfInfos != null)
            {
                InitWithResponseInfo();
            }
        }
    }
    public List<FaceBookPlayerInfo> GetFaceBookFriendList()
    {
        return faceBookFriendList;
    }
    public FaceBookPlayerInfo GetSelfInfo()
    {
        return selfInfo;
    }
    private void InitWithResponseInfo()
    {
        if (_MySelfInfos == null)
            return;
        selfInfo.bIsPicChanged = false;
        selfInfo.Name       = _MySelfInfos.name;
        selfInfo.Email      = _MySelfInfos.email;
        selfInfo.picData    = _MySelfInfos.picture.data;
        selfInfo.PlayerId   = _MySelfInfos.id;

        for (int i = 0; i < _MySelfInfos.friends.data.Count; ++i)
        {
            Friend fInfo = _MySelfInfos.friends.data[i];
            FaceBookPlayerInfo temp = GetFriendItemInList(fInfo.id);
            if (temp != null)
            {
                temp.Email = "";
                temp.bIsPicChanged = false;
                if (temp.picData.url != fInfo.picture.data.url)
                    temp.bIsPicChanged = true;
                temp.picData = fInfo.picture.data;
                temp.PlayerId = fInfo.id;
                temp.Name = fInfo.name;
            }
            else
            {
                temp = new FaceBookPlayerInfo();
                temp.Email = "";
                temp.bIsPicChanged = false;
                temp.picData = fInfo.picture.data;
                temp.PlayerId = fInfo.id;
                temp.head = null;
                temp.Name = fInfo.name;
                faceBookFriendList.Add(temp);
            }
        }
    }
    private FaceBookPlayerInfo GetFriendItemInList(string id)
    {
        for (int i = 0; i < faceBookFriendList.Count; ++i)
        {
            if (id == faceBookFriendList[i].PlayerId)
            {
                return faceBookFriendList[i];
            }
        }
        return null;
    }
    public void GetMeInfo(Action<bool, string> callBack)
    {
        _GetMeInfoCallback = callBack;

        if (_MySelfInfos == null)
        {
            _MySelfInfos = SdkFileHelper.LoadFaceBookDataFromJson<ResponseInfo>(FaceBookLocalFileName);

            if (_MySelfInfos != null)
            {
                InitWithResponseInfo();
                if(_GetMeInfoCallback != null)
                    _GetMeInfoCallback(true, "");
            }
        }
        else
        {
            if (_GetMeInfoCallback != null)
                _GetMeInfoCallback(true, "");
        }
        FB.API("me?fields=id,name,picture,email,friends", HttpMethod.GET, this.GetMeInfoResult);
    }
    void GetMeInfoResult(IResult result)
    {
        if (result == null)
        {
            if (_MySelfInfos == null)
                _GetMeInfoCallback(false, "get me info fail");
            return;
        }
        if (!string.IsNullOrEmpty(result.Error))
        {
            if (_MySelfInfos == null)
                _GetMeInfoCallback(false, result.Error);
            return;
        }
        if (!string.IsNullOrEmpty(result.RawResult))
        {
            try
            {
                bool bCallback = false;
                if (_MySelfInfos == null)
                    bCallback = true;
                _MySelfInfos = JsonUtility.FromJson<ResponseInfo>(result.RawResult);
                string path = SdkFileHelper.FaceBookPlayerInfoPath();
                SdkFileHelper.SaveFaceBookDataToJson<ResponseInfo>(_MySelfInfos, FaceBookLocalFileName);
                InitWithResponseInfo();
                if(bCallback)
                    _GetMeInfoCallback(true, result.RawResult);
            }
            catch (Exception ex)
            {
                if (_MySelfInfos == null)
                    _GetMeInfoCallback(false, ex.ToString());
            }

            return;
        }
        if (_MySelfInfos == null)
            _GetMeInfoCallback(false, "facebook do not get myself info ,error don't know");
    }

    //private IEnumerator TakeScreenshot()
    //{
    //    yield return new WaitForEndOfFrame();

    //    var width = Screen.width;
    //    var height = Screen.height;
    //    var tex = new Texture2D(width, height, TextureFormat.RGB24, false);

    //    // Read screen contents into the texture
    //    tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
    //    tex.Apply();
    //    byte[] screenshot = tex.EncodeToPNG();

    //    var wwwForm = new WWWForm();
    //    wwwForm.AddBinaryData("image", screenshot, "InteractiveConsole.png");
    //    wwwForm.AddField("message", "herp derp.  I did a thing!  Did I do this right?");
    //    //FB.API("me/photos", HttpMethod.POST, this.HandleResult, wwwForm);
    //}
    public void GetFaceBookFrientList()
    {
        Dictionary<string, string> requestData = new Dictionary<string, string>();
        FB.API("", HttpMethod.GET, FriendCallBack, requestData);
    }
    void FriendCallBack(IGraphResult result)
    {

    }
}
public class FaceBookPlayerInfo
{
    public bool         bIsPicChanged     = false;
    public string       Email       = "";
    public string       PlayerId    = "";
    public string       Name        = "";
    public pictureData  picData     = null;
    public Texture2D    head        = null;

    private Action<FaceBookPlayerInfo> LoadTextureCallBack;

    public FaceBookPlayerInfo()
    {
        bIsPicChanged   = false;
        Email           = "";
        PlayerId        = "";
        picData         = null;
        head            = null;
        Name            = "";
    }

    public void LoadPicture(Action<FaceBookPlayerInfo> callBack)
    {
        LoadTextureCallBack = callBack;
        bool bIsNeedLoadImage = false;
        if (bIsPicChanged == true)
            bIsNeedLoadImage = true;
        else if (head == null)
        {
            //先检测本地是否有图片文件
            head = SdkFileHelper.ReadFaceBookTexture(PlayerId);
            if (head != null)
            {
                LoadTextureCallBack(this);
            }
            else
            {
                bIsNeedLoadImage = true;
                
            }
        }
        if(bIsNeedLoadImage)
            FB.API("/" + PlayerId + "/picture?type=small", HttpMethod.GET, this.ProfilePhotoCallback);
    }
    Texture2D ScaleTexture(Texture2D source, int targetWidth, int targetHeight)
    {
        Texture2D result = new Texture2D(targetWidth, targetHeight, source.format, false);

        float incX = (1.0f / (float)targetWidth);
        float incY = (1.0f / (float)targetHeight);

        for (int i = 0; i < result.height; ++i)
        {
            for (int j = 0; j < result.width; ++j)
            {
                Color newColor = source.GetPixelBilinear((float)j / (float)result.width, (float)i / (float)result.height);
                result.SetPixel(j, i, newColor);
            }
        }

        result.Apply();
        return result;
    }
    private void ProfilePhotoCallback(IGraphResult result)
    {
        if (string.IsNullOrEmpty(result.Error) && result.Texture != null)
        {
#if UNITY_IOS
            head = ScaleTexture(result.Texture,256,256);
#else
            head = ScaleTexture(result.Texture, 128, 128);
#endif
            //head.Resize(64, 64);
            SdkFileHelper.SaveFaceBookTexture(head, PlayerId);
        }
        else
        {
            Debug.LogError("face book get phote fail error =" + result.Error);
        }
        if (LoadTextureCallBack != null)
        {
            LoadTextureCallBack(this);
        }
    }
}