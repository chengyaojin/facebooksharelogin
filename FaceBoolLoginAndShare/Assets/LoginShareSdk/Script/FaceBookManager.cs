using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceBookManager : MonoBehaviour
{
    private static FaceBookManager _instance = null;
    public static FaceBookManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject obj = new GameObject("FaceBookManager");
                DontDestroyOnLoad(obj);
                _instance = obj.AddComponent<FaceBookManager>();
            }
            return _instance;
        }
        private set
        {
            _instance = value;
        }
    }



    private Action _CompleteCallback = null;
    
    private Action<bool, string> _ShareCallback = null;
    private Action<bool, string> _GetMeInfoCallback = null;


    private FaceLoginController faceLoginController     = new FaceLoginController();
    private FaceShareController faceShareController     = new FaceShareController();
    private FacePlayerController facePlayerController   = new FacePlayerController();

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        Init(null);
    }
    #region 初始化相关
    public bool IsInitialized()
    {
        return FB.IsInitialized;
    }
    private void Init(System.Action CompleteCallBack)
    {
        _CompleteCallback = CompleteCallBack;
        FB.Init(this.OnInitComplete, this.OnHideUnity);
        facePlayerController.InitFriendList();
    }

    private void OnInitComplete()
    {
        string assessToken = "";
        if (AccessToken.CurrentAccessToken != null)
        {
            assessToken = AccessToken.CurrentAccessToken.ToString();
        }
        Debug.Log("face init finish assessToken =" + assessToken);
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (_CompleteCallback != null)
        {
            _CompleteCallback();
        }
        
    }
    #endregion

    #region 登录接口

    public bool IsdLogedInFaceBook()
    {
        if (AccessToken.CurrentAccessToken != null)
        {
            //检测token过期时间
            int nResult = DateTime.Compare(AccessToken.CurrentAccessToken.ExpirationTime, DateTime.Now);
            if (nResult > 0)
                return true;
        }
        return false;
    }
    //刷新token
    public void RefreshFaceBookToken(Action<bool, string> callback)
    {
        faceLoginController.RefreshToken(callback);
    }
    //玩家登录
    public void LoginFaceBookWithFriendPermision(Action<bool,string> callback)
    {
        if (IsInitialized() == false)
        {
            if (callback != null)
            {
                callback(false,"facebook sdk no init");
            }
            return;
        }
        faceLoginController.LoginWithFriend(callback);
    }

    public void FaceBookLogout()
    {
        FB.LogOut();
    }
    #endregion



    #region 分享相关
    public void ShareLink(string LinkUrl, Action<bool, string> HandleCallBack)
    {
        faceShareController.ShareLink(LinkUrl, HandleCallBack);
    }
    public void FeedShare(string toId = "", Uri link = null, Uri picture = null, string mediaSource = "", Action<bool, string> HandleCallBack = null)
    {
        faceShareController.FeedShare(toId,link,picture,mediaSource,HandleCallBack);
    }
    public void ShareImageToFaceBook(Texture2D tex, Action<bool, string> ShareImageCallback)
    {
        faceShareController.ShareImageToFaceBook(tex, ShareImageCallback);
    }

    #endregion

    #region 获取好友相关
    public void GetMeInfo(Action<bool, string> callBack)
    {
        facePlayerController.GetMeInfo(callBack);
    }
    public FaceBookPlayerInfo GetSelfInfo()
    {
        return facePlayerController.GetSelfInfo();
    }
    public List<FaceBookPlayerInfo> GetFaceBookFriendList()
    {
        return facePlayerController.GetFaceBookFriendList();
    }
    #endregion
}
