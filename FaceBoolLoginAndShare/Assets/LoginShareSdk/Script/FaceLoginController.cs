using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceLoginController
{
    enum Scope
    {
        PublicProfile = 0b_0000_0001,
        UserFriends = 0b_0000_0010,
        UserBirthday = 0b_0000_0100,
        UserAgeRange = 0b_0000_1000,
        PublishActions = 0b_0001_0000,
        UserLocation = 0b_0010_0000,
        UserHometown = 0b_0100_0000,
        UserGender = 0b_1000_0000,
    };
    private Action<bool, string> _LoginCallback = null;
    private Action<bool, string> _RefreshTokenCallBack = null;
    #region 登录相关
    public void LoginWithFriend(Action<bool, string> callback)
    {
        _LoginCallback = callback;
        //未初始化
        if (FaceBookManager.Instance.IsInitialized() == false)
        {
            if (callback != null)
            {
                callback(false, "facebook has not init!");
            }
            return;
        }

        this.CallFBLogin(Scope.PublicProfile | Scope.UserFriends);
    }
    public void RefreshToken(Action<bool, string> callback)
    {
        _RefreshTokenCallBack = callback;
        if (AccessToken.CurrentAccessToken != null)
        {
            //查看token过期时间，如果小于一定时间，则走刷新token流程
            //首先检测token过期时间
            int nResult = DateTime.Compare(AccessToken.CurrentAccessToken.ExpirationTime, DateTime.Now);
            if (nResult > 0)
            {
                //走刷新token 静默刷新流程
                FB.Mobile.RefreshCurrentAccessToken(this.RefreshTokenResult);
                return;
            }
            //否则走登录流程
        }
        if (_RefreshTokenCallBack != null)
            _RefreshTokenCallBack(false,"refresh token failed in Refresh Token function");
    }
    //刷新token
    private void RefreshTokenResult(IResult result)
    {
        if (result == null)
        {
            if (_RefreshTokenCallBack != null)
                _RefreshTokenCallBack(false, "refresh token failed result = null");
            return;
        }

        // Some platforms return the empty string instead of null.
        if (!string.IsNullOrEmpty(result.Error))
        {
            if (_RefreshTokenCallBack != null)
                _RefreshTokenCallBack(false, result.Error);
            return;
        }

        if (!string.IsNullOrEmpty(result.RawResult))
        {
            if (_RefreshTokenCallBack != null)
                _RefreshTokenCallBack(true, result.RawResult);
            return;
        }
        if (_RefreshTokenCallBack != null)
            _RefreshTokenCallBack(false, "refresh token failed no reason");
    }

    //调用登录
    private void CallFBLogin(Scope scopemask)
    {
        List<string> scopes = new List<string>();

        if ((scopemask & Scope.PublicProfile) > 0)
        {
            scopes.Add("public_profile");
        }
        if ((scopemask & Scope.UserFriends) > 0)
        {
            scopes.Add("user_friends");
        }
        if ((scopemask & Scope.UserBirthday) > 0)
        {
            scopes.Add("user_birthday");
        }
        if ((scopemask & Scope.UserAgeRange) > 0)
        {
            scopes.Add("user_age_range");
        }

        if ((scopemask & Scope.UserLocation) > 0)
        {
            scopes.Add("user_location");
        }

        if ((scopemask & Scope.UserHometown) > 0)
        {
            scopes.Add("user_hometown");
        }

        if ((scopemask & Scope.UserGender) > 0)
        {
            scopes.Add("user_gender");
        }

        FB.LogInWithReadPermissions(scopes, AuthCallback);
    }

    //登录回调
    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.LogError(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.LogError(perm);
            }
            if (_LoginCallback != null)
            {
                //在这里应该去申请个人信息及好友信息，申请结束后，再回调
                _LoginCallback(true, "");
            }
        }
        else
        {
            Debug.LogError("User cancelled login error =" + result.Error);
            if (_LoginCallback != null)
            {
                _LoginCallback(false, result.Error);
            }
        }
    }
    #endregion
}
