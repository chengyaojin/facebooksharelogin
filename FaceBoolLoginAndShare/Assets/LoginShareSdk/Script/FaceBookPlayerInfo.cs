using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResponseInfo
{
    
    public string id;
    public string name;
    public string email;
    public pictureInfo picture;

    public FriendInfoList friends;

}

[Serializable]
public class pictureInfo
{
    public pictureData data;
}

[Serializable]
public class pictureData
{
    public int height;
    public bool is_silhouette;
    public string url;
    public int width;
}

[Serializable]
public class Friend
{
    public string id;
    public string name;
    public pictureInfo picture;
}

[Serializable]
public class FriendInfoList
{
    public List<Friend> data;
}