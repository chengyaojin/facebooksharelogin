using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FaceBookUIDemo : MonoBehaviour
{

    public Text InitResultLabel;
    public Button LoginButton;
    public Image MeHead;
    public Text MeID;
    public Text MeName;
    public Text MeEmail;
    public Button GetFriendList;
    public ScrollRect scrollView;

    public friendItemInfo ScrollObject;
    public Transform content;

    public Button ShareLinkBtn;

    public Button FBLogOut;

    public Button ShareImageBtn;
    private List<friendItemInfo> childList = new List<friendItemInfo>();
    // Start is called before the first frame update
    void Start()
    {
        LoginButton.onClick.AddListener(onClickLoginButton);
        GetFriendList.onClick.AddListener(onClickGetFrientList);
        ShareLinkBtn.onClick.AddListener(onClickShareLink);
        FBLogOut.onClick.AddListener(FBLogOutButtonClick);
        ShareImageBtn.onClick.AddListener(ShareImageBtnClick);
    }
    //登入
    void onClickLoginButton()
    {
        if (FaceBookManager.Instance.IsdLogedInFaceBook())
        {
            FaceBookManager.Instance.RefreshFaceBookToken(null);
            loginCallBack(true, "");
        }
        else
        {
            FaceBookManager.Instance.LoginFaceBookWithFriendPermision(loginCallBack);
        }
    }

    void loginCallBack(bool bFlag, string Info)
    {
        if (bFlag == true)
        {
            LoginButton.interactable = false;

        }
        else
        {
            LoginButton.interactable = true;
            Debug.LogError("facebook login fail error =" + Info);
        }
    }

    //登出
    void FBLogOutButtonClick()
    {
        FaceBookManager.Instance.FaceBookLogout();
        LoginButton.interactable = true;
        for (int i = childList.Count - 1; i >= 0; i--)
        {
            //childList[i];
            childList[i].transform.SetParent(null);
            GameObject.Destroy(childList[i]);//这里移除的是transform组件，运行的时候会报错
        }
        childList.Clear();
        MeID.text       = "ID: ";
        MeName.text     = "Name: ";
        MeEmail.text    = "Email: ";
        MeHead.sprite = null;
        GetFriendList.interactable = true;
    }

    //分享链接
    void onClickShareLink()
    {
        FaceBookManager.Instance.ShareLink("https://google.com", shareLinkCallBack);
    }
    void shareLinkCallBack(bool ret ,string info)
    {
        Debug.LogError("face book share link error =" + info + " isSucc ="+ ret);
    }

    //上传图片
    void ShareImageBtnClick()
    {
        if (FaceBookManager.Instance.IsdLogedInFaceBook() == false)
            return;
        FaceBookPlayerInfo info = FaceBookManager.Instance.GetSelfInfo();
        FaceBookManager.Instance.ShareImageToFaceBook(info.head, ShareImageCallBack);
    }
    void ShareImageCallBack(bool isSucc, string info)
    {
        Debug.LogError("shengming shareimage ret = " + info + " isSucc =" + isSucc);
    }

    //获取个人信息
    void onClickGetFrientList()
    {
        FaceBookManager.Instance.GetMeInfo(getFriendListCallBack);
    }
    void getFriendListCallBack(bool bFlag, string Info)
    {
        if (bFlag == true)
        {
            GetFriendList.interactable = false;

            //设置个人信息
            FaceBookPlayerInfo info = FaceBookManager.Instance.GetSelfInfo();
            MeID.text       = "ID: " + info.PlayerId;
            MeName.text     = "Name: " + info.Name;
            MeEmail.text    = "Email: " + info.Email;
            if (info.head != null)
            {
                MeHead.sprite = Sprite.Create(info.head, new Rect(0, 0, info.head.width, info.head.height), Vector2.zero);
            }
            else
            {
                info.LoadPicture(FaceBookLoadPictureCallBack);
            }
            //设置好友信息
            List<FaceBookPlayerInfo> friendList = FaceBookManager.Instance.GetFaceBookFriendList();
            for (int i = 0; i < friendList.Count; ++i)
            {
                friendItemInfo item = Instantiate(ScrollObject, content);
                item.Init(friendList[i]);
                item.gameObject.SetActive(true);
                childList.Add(item);
            }
        }
        else
        {
            GetFriendList.interactable = true;
            Debug.LogError("facebook login fail error =" + Info);
        }
    }
    void FaceBookLoadPictureCallBack(FaceBookPlayerInfo info)
    {
        if (info.head != null)
        {
            MeHead.sprite = Sprite.Create(info.head, new Rect(0, 0, info.head.width, info.head.height), Vector2.zero);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
