using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class friendItemInfo : MonoBehaviour
{
    public Image Head;
    public Text Name;
    public Text ID;
    public Button Share;

    private FaceBookPlayerInfo selfInfo;

    public void Init(FaceBookPlayerInfo info)
    {
        selfInfo = info;
        if (info.head != null)
        {
            Head.sprite = Sprite.Create(info.head, new Rect(0, 0, info.head.width, info.head.height), Vector2.zero);
        }
        else
        {
            info.LoadPicture(FaceBookLoadPictureCallBack);
        }

        Name.text   = "Name: " + selfInfo.Name;
        ID.text     = "ID: " + selfInfo.PlayerId;
    }

    void FaceBookLoadPictureCallBack(FaceBookPlayerInfo info)
    {
        if (info.head != null)
        {
            Head.sprite = Sprite.Create(info.head, new Rect(0, 0, info.head.width, info.head.height), Vector2.zero);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Share.onClick.AddListener(ShareButtonClicked);    
    }
    void ShareButtonClicked()
    {
        FaceBookManager.Instance.FeedShare(selfInfo.PlayerId,new System.Uri("https://www.baidu.com"),null, "", callback);
    }
    void callback(bool ret, string info)
    {
        if (ret != true)
        {
            Debug.LogError("facebook share error ="+info);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
